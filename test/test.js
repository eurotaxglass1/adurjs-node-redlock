let assert = require('chai').assert;
let RedlockManager = require('../src/redlock-manager');
let Redlock = require('redlock');
const _ = require('underscore');

const lockOptions = override => (
  {
    driftFactor: 0.01,
    retryCount: 2,
    retryDelay: 150,
    retryJitter: 50,
    ttl: 200,
    ...override
  }
);

test('single-server: https://www.npmjs.com/package/redis', {'clients':[ {'name': 'redis'} ]});
test('single-server: https://www.npmjs.com/package/redis (string_numbers=true)',
  {'clients':[ {'name': 'redis', 'options': { 'string_numbers': true }} ]});
test('single-server: https://www.npmjs.com/package/ioredis', {'clients': [ {'name': 'ioredis'} ]});
test('multi-server: https://www.npmjs.com/package/ioredis',
  {'clients': [
    {'name': 'ioredis', 'options': { 'db': 1 }},
    {'name': 'ioredis', 'options': { 'db': 2 }},
    {'name': 'ioredis', 'options': { 'db': 3 }}
  ]});

function test (name, configExtension) {
  let redlockManager = new RedlockManager(_.extend({},lockOptions(), configExtension), console);
  let resourceString = 'RedlockManager:resource';
  let resourceArray = [ 'RedlockManager:resource1', 'RedlockManager:resource2' ];
  let error = 'RedlockManager:error';

  describe('RedlockManager: ' + name, function () {
    // it('should throw an error if not passed any clients', function () {
    // 	assert.throws(function () {
    // 		new RedlockManager([], {
    // 			retryCount: 2,
    // 			retryDelay: 150,
    // 			retryJitter: 0,
    // 			ttl:200
    // 		});
    // 	});
    // });

    it('supports custom script functions in options', function () {
      let lockScript = `
			for i, key in ipairs(KEYS) do
				if redis.call('setnx',KEYS[1],ARGV[1])==1  then  
				  if redis.call('pexpire', KEYS[1],ARGV[2])==1 then 
					-- Return the number of entries updated.
					return #KEYS
				  end   
				end 
				return '0';
			end
			`;

      let opts = {
        lockScript: function (lockScript) { return lockScript; },
        unlockScript: function (unlockScript) { return unlockScript + 'and 2'; },
        extendScript: function (extendScript) { return extendScript + 'and 3'; },
        ttl: 200
      };
      let customRedlock = new RedlockManager(opts, console);
      let i = 1;
      assert.equal(customRedlock.Redlock.lockScript, redlockManager.Redlock.lockScript);
      i++;
      //assert.equal(customRedlock.Redlock.unlockScript, redlockManager.Redlock.unlockScript + 'and ' + i++);
      //assert.equal(customRedlock.Redlock.extendScript, redlockManager.Redlock.extendScript + 'and ' + i++);

    });

    describe('promises', function () {

      before(function (done) {
        let err;
        let l = clients.length;

        function cb (e) {
          if (e) err = e;
          l--;
          if (l === 0) done(err);
        }

        for (let i = clients.length - 1; i >= 0; i--) {
          clients[ i ].del(resourceString, cb);
        }
      });

      let one;
      it('should lock a resource', async function () {
        redlockManager.setTTL(200);
        let lock = await redlockManager.lock(resourceString);
        assert.isObject(lock);
        assert.instanceOf(lock, Redlock.Lock);
        assert.isAbove(lock.expiration, Date.now() - 1);
        assert.equal(lock.attempts, 1);
        one = lock;
      });

      let two;
      let two_expiration;
      it('should wait until a lock expires before issuing another lock', async function () {
        assert(one, 'Could not run because a required previous test failed.');
        redlockManager.setTTL(800)
        let lock = await redlockManager.lock(resourceString);
        assert.isObject(lock);
        assert.isAbove(lock.expiration, Date.now() - 1);
        assert.isAbove(Date.now() + 2000, one.expiration);
        assert.isAbove(lock.attempts, 1);
        two = lock;
        two_expiration = lock.expiration;
      });

      it('should unlock a resource', async function () {
        assert(two, 'Could not run because a required previous test failed.');
        await two.unlock();
        assert.equal(two.expiration, 0, 'Failed to immediately invalidate the lock.');
      });

      it('should unlock an already-unlocked resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        two.unlock().done(function (result) {
          done(new Error('Expected an error.'));
        }, function (err) {
          done();
        });
      });

      it('should error when unable to fully release a resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        let failingTwo = Object.create(two);
        failingTwo.resource = error;
        failingTwo.unlock().done(done, function (err) {
          assert.isNotNull(err);
          done();
        });
      });

      it('should fail to extend a lock on an already-unlocked resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        two.extend(2000)
          .done(function () {
            done(new Error('Should have failed with a LockError'));
          }, function (err) {
            assert.instanceOf(err, Redlock.LockError);
            assert.equal(err.attempts, 0);
            done();
          });
      });

    });

    describe('promises - multi', function () {

      before(function (done) {
        let err;
        let l = clients.length;

        function cb (e) {
          if (e) err = e;
          l--;
          if (l === 0) done(err);
        }

        for (let i = clients.length - 1; i >= 0; i--) {
          for (let j = resourceArray.length - 1; j >= 0; j--) {
            clients[ i ].del(resourceArray[ j ], cb);
          }
        }
      });

      let one;
      it('should lock a multivalue resource', async function () {
        redlockManager.setTTL(200)
        let lock = await redlockManager.lock(resourceArray);
        assert.isObject(lock);
        assert.isAbove(lock.expiration, Date.now() - 1);
        assert.equal(lock.attempts, 1);
        one = lock;
      });

      let two;
      let two_expiration;
      it('should wait until a multivalue lock expires before issuing another lock', async function () {
        assert(one, 'Could not run because a required previous test failed.');
        redlockManager.setTTL(800)
        let lock = await redlockManager.lock(resourceArray);
        assert.isObject(lock);
        assert.isAbove(lock.expiration, Date.now() - 1);
        assert.isAbove(Date.now() + 1, one.expiration);
        assert.isAbove(lock.attempts, 1);
        two = lock;
        two_expiration = lock.expiration;
        await two.unlock();
      });

      it('should unlock an already-unlocked multivalue resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        two.unlock().done(function (result) {
          done(new Error('Expected an error.'));
        }, function (err) {
          done();
        });
      });

      it('should error when unable to fully release a multivalue resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        let failingTwo = Object.create(two);
        failingTwo.resource = error;
        failingTwo.unlock().done(done, function (err) {
          assert.isNotNull(err);
          done();
        });
      });

      it('should fail to extend a lock on an already-unlocked multivalue resource', function (done) {
        assert(two, 'Could not run because a required previous test failed.');
        two.extend(2000)
          .done(function () {
            done(new Error('Should have failed with a LockError'));
          }, function (err) {
            assert.instanceOf(err, Redlock.LockError);
            assert.equal(err.attempts, 0);
            done();
          });
      });

      after(function (done) {
        let err;
        let l = clients.length;

        function cb (e) {
          if (e) err = e;
          l--;
          if (l === 0) done(err);
        }

        for (let i = clients.length - 1; i >= 0; i--) {
          for (let j = resourceArray.length - 1; j >= 0; j--) {
            clients[ i ].del(resourceArray[ j ], cb);
          }
        }
      });
    });

    describe('quit', function () {

      it('should quit all clients', async function () {
        let results = await redlockManager.Redlock.quit();
        assert.isArray(results);
      });
    });

  });
}








