'use strict';

const _ = require('underscore');
const redis = require('redis');
const REDIS = 'redis';
const ioredis = require('ioredis');
const IOREDIS = 'ioredis';

const CONNECTION_BROKEN = 'CONNECTION_BROKEN';
const RECONNECTION_FREQUENCY = 5000;
const AFTER_RECONNECTION_DELAY = 2000;

const DEFAULT_RETRY_STRATEGY = function (options) {
  if (options.error) {
    if (options.error.code === 'ECONNREFUSED') {
      // End reconnecting on a specific error and flush all commands with a individual error
      return new Error('The server refused the connection');
    }
    if (options.error.code === 'ECONNRESET') {
      return new Error('The server reset the connection');
    }
    if (options.error.code === 'ETIMEDOUT') {
      return new Error('The server timeouted the connection');
    }
  }
};

class RedisManager {

  constructor(log, clientsConfig, onReconnect){
    this.log = log;
    this.clientsConfig = clientsConfig;
    this.onReconnect = onReconnect;
    this.createClients();
  }

  getClients(){
    return this.clients;
  }

  createClients() {
    if (_.isUndefined(this.clientsConfig) || _.isNull(this.clientsConfig) || !_.isArray(this.clientsConfig)) {
      return [];
    }

    this.clients =  _.map(this.clientsConfig, (clientConfig) => {
      let client = createClient(clientConfig);
      if (_.isUndefined(client)) {
        throw new Error('Client name not supported: "' + clientConfig.name + '".');
      }
      client.on('error', (error) => {
        if (CONNECTION_BROKEN === error.code) {
          this.reconnect();
        }
      });
      return client;
    });
  }

  reconnect(){
    let id = setInterval(() => {
      this.log.info('Reconnecting...');
      this.clients = _.map(this.clients, (client, index) => {
        if (client.connected){
          return client;
        }
        let newClient = createClient(this.clientsConfig[index]);
        newClient.on('error', (err) => {
          this.log.warn(err.message);
        });
        return newClient;
      });

      setTimeout((clients) =>{

        if (_.every(clients, (client) => client.connected)){
          _.each(clients, (client) => {
            client.on('error', (error) => {
              if (CONNECTION_BROKEN === error.code) {
                this.reconnect();
              }
            });
          });
          this.log.info('Reconnected.');
          this.onReconnect(this.clients);
          clearInterval(id);
        }
      }, AFTER_RECONNECTION_DELAY, this.clients);
    },RECONNECTION_FREQUENCY);
  }
}

function createClient (clientConfig){
  if (clientConfig.name === REDIS) {
    return redis.createClient(clientConfig.port, clientConfig.host, _.extend(clientConfig.options || {}, { 'retry_strategy': DEFAULT_RETRY_STRATEGY }));
  }
  if (clientConfig.name === IOREDIS) {
    return clientConfig.options ? ioredis(clientConfig.options) : ioredis();
  }
  return undefined;
}

module.exports = RedisManager;
