const RedlockBase = require('redlock');
const _ = require('underscore');
const RedisManager = require('./redisManager');

const lockScript = `
for i, key in ipairs(KEYS) do
    if redis.call('setnx', key, ARGV[1])==1  then  
      redis.call('pexpire', key, ARGV[2])
    else
      return 0
    end 
end
return #KEYS 
`;
const unlockScript = `
local count = 0
for i, key in ipairs(KEYS) do
  -- Only remove entries for *this* lock value.
  if redis.call("get", key) == ARGV[1] then
    redis.pcall("del", key)
    count = count + 1
  end
end

-- Return the number of entries removed.
return count
`;

// defaults
const defaults = {
  driftFactor: 0.01,
  retryCount: 10,
  retryDelay: 200,
  retryJitter: 100,
  ttl: 4000,
};

class RedlockManager {


  constructor(options, log) {

    this.log = log;

    // Disabling RedlockManager must be done explicitly by setting option 'enabled': false
    if (typeof options.enabled !== 'boolean' || options.enabled !== false) {

      let onReconnect = (clients) => { this.Redlock = new RedlockBase(clients, this.options);};
      let clients = new RedisManager(log, options.clients, onReconnect).getClients();

      if (_.isEmpty(clients)) {
        throw new Error('Clients configuration not provided.');
      }

      if (_.isUndefined(options) || _.isNull(options)) {
        let options = {};
        options.lockScript = () => { return lockScript; };
        options.unlockScript = () => { return unlockScript; };
        options.driftFactor = defaults.driftFactor;
        options.retryCount = defaults.retryCount;
        options.retryDelay = defaults.retryDelay;
        options.retryJitter = defaults.retryJitter;
        this.ttl = defaults.ttl;

      } else {
        this.ttl = _.isNumber(options.ttl) ? options.ttl : defaults.ttl;

        options.lockScript = (_.isUndefined(options.lockScript) || _.isEmpty(options.lockScript))
          ? () => { return lockScript; }
          : options.lockScript;

        options.unlockScript = (_.isUndefined(options.unlockScript) || _.isEmpty(options.unlockScript))
          ? () => { return unlockScript; }
          : options.unlockScript;
      }
      this.options = options;
      this.Redlock = new RedlockBase(clients, options);

      this.Redlock.on('clientError', (error) => {
        this.log.warn('clientError: ' + error.message);
      });

      this.errorHandlers = [];

      this._processError = this._processError.bind(this);

    }
  }

  async lock(resource) {
    if (this.Redlock)
      return await this.Redlock.lock(resource, this.ttl);
  }

  async unlock(lock) {
    if (this.Redlock)
      return await this.Redlock.unlock(lock);
  }

  async extend(lock, ttl) {
    if (this.Redlock)
      return await this.Redlock.extend(lock, ttl);
  }

  async quit() {
    if (this.Redlock)
      return await this.Redlock.quit();
  }

  setTTL(ttl) {
    if (this.Redlock)
      this.ttl = ttl;
  }
  _processError(error) {
    for (let handler of this.errorHandlers) {
      try {
        handler(error);
      } catch ({ message }) {
        this.log.error(`LockManager error handler generated error. ${error.message}`);
      }
    }
  }

  addErrorHandler(handler) {
    this.errorHandlers.push(handler);
  }

  removeErrorHandler(handler) {
    const index = this.errorHandlers.findIndex(handler);
    if (index === -1) {
      this.log.warn('Cannot remove error handler, not found');
      return;
    }
    this.errorHandlers.splice(index, 1);
  }

}

module.exports = RedlockManager;
