var RedlockManager = require('../src/redlock-manager');
const _ = require('underscore');
var clients = [require('redis').createClient(6379,"localhost")]

const lockOptions = override => ({
    driftFactor: 0.01,
    retryCount: 2,
    retryDelay: 150,
    retryJitter: 50,
    ttl: 60000,
    ...override
}
);

// you should have one client for each independent redis node  or cluster
var redlockManager = new RedlockManager(clients, lockOptions());
//var redlockManager = new RedlockManager();

async function lockSingleResource() {

    var resource = 'subject-ck0oxvvun03ew07679hp4sj6';

    try {
        let lockResult = await redlockManager.lock(resource);
        console.log(lockResult);

        //let lockResult2 = await redlockManager.lock(resource);
        
        // ...perform some locked acti//on here...
        // e.g. await this.redisClient.set(key, JSON.stringify(value))
        // e.g. await updateDatabaseTable(rowData);

        // unlock your resource when you are done checking the above action worked okay.
        var unlockResult = lockResult.unlock();

    } catch (err) {
        console.log('Error LockManager : ' + err.stack, err);
        return err;
    }
}

async function runSingle() {
    let response = await lockSingleResource();
}

//runSingle();


async function lockMultipleResource() {

    var resourceArray = ['subject-ck2ornr8l00c007845fmzfdoy', 'subject-ck5ma2rtm06830784crbdczaj','subject-ck1w1lmwq00g80784m1arra0r'];
    try {
        var lockResult = await redlockManager.lock(resourceArray);
        console.log(lockResult);

        //var lockResult2 = await redlockManager.lock(resourceArray);

        // ...perform some locked action here...
        // e.g. await this.redisClient.set(key, JSON.stringify(value))
        // e.g. await updateDatabaseTable(rowData);

        // unlock your resource when you are done checking the above action worked okay.
        var unlockResult = lockResult.unlock();

        //let quitResults = await redlockManager.quit();
    } catch (err) {
        console.log('Error LockManager : ' + err.stack, err);
        return err;
    }

}

async function runMultiple() {
    let response = await lockMultipleResource();
}

runMultiple();

